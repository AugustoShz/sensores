# Projeto de estagio para o Smart Campus

Projeto realizada para processo de estagio no Smart Campus.


## Resumo

O projeto consiste em enviar um POST request a um servidor PHP que fará a permanencia dos dados enviados em um banco de dados MySQL.


## Linguagens utilizadas

### 1. PHP
 O PHP foi utilizado como API para a permanencia dos dados no banco, utilizando o PDO para a comunicação entre eles.
 Os dados foram enviados no formato JSON através do Body e o ID do sensor pelo Header no seguinte formato:

> -H “SensorID: [ID]”
> -BODY ‘{"date": "yyyy-mm-dd hh:mm", "prediction": int}’

### 2. Python
Com o python foi realizado o POST, deixando o mais generico possivel para ser utilizado para qualquer servidor, recebendo o caminho até o servidor diretamente do usuário.

**Bibliotecas utilizadas:**

    Random
    Requests
    Datetime
    
## Banco de dados

Foi utilizado o banco de dados MySQL para armazenamento dos dados enviados.

**Estrutura das tabelas**

Sensor | Info
------------ | -------------
SensorID (PK) | ID (PK)
SensorName | SensorInfo
NONE | infoDate
NONE | SensorID (FK)


## Rodando a Aplicação

Para a execução da aplicação será necessário configurar um servidor PHP de sua preferencia para a hospedagem do arquivo "PHPscript.php", foi utilizado originalmente o servidor Apache2 para linux.
Python 3 é necessário para rodar o arquivo "Pythonscript.py".