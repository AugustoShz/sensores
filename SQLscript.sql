create database Sensores;
use Sensores;

create table sensor(
sensorID int primary key,
sensorName varchar(50)
);

create table info(
id int primary key auto_increment,
sensorInfo int,
infoDate datetime,
sensorID int
);

ALTER TABLE info
ADD CONSTRAINT fk_sensorID_info
FOREIGN KEY (sensorID)
REFERENCES sensor(sensorID);