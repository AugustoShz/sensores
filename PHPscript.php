<?php  
//Pegando os headers enviados no request
$headers = apache_request_headers();

//Variaveis
$SensorId = NULL;
$servername = "172.16.231.2:3306";
$username = "estagio";
$password = "estagio";
$dbname = "Sensores";

//Atribuindo o ID do sensor se no request foi enviado o header "SensorID"
foreach ($headers as $header => $value) {
    if($header == "SensorID"){
        $SensorId = $value;
        break;
    }
}

//Verificação se o SensorId possui valor para a inserção dos dados no banco
if(!$SensorId == NULL){
    //Recebendo o JSON enviado pelo body do Request
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    //Variavel de sinalização da existencia do sensor no banco
    $count = FALSE;
    
    try{
        //Criando a conexão com o banco de dados
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

        //Adicionando opções à conexão
        //ATTR_ERRMODE = Modo de report de erros no banco
        //ERRMODE_EXCEPTION = O erro será e reportado com uma exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //Select do SensorID
        $sensorExistsQuery = "SELECT SensorId FROM Sensor WHERE SensorId = " . $SensorId ;
        $result = $conn->query($sensorExistsQuery);

        //Caso encontre qualquer resultado, a variavel count será mudada para TRUE
        foreach($result as $row) {
            $count = TRUE;
            break;
        }

        //Se count for true o sensor já existe no banco, caso não exista ele é cadastrado
        if ($count) {
            echo "Sensor ID was found at database \n";
        }
        else{
            echo "Sensor not found, adding to database\n";
            $sql = "INSERT INTO Sensor (SensorId, SensorName) VALUES (" . $SensorId . ", 'Sensor" . $SensorId ."')";
            $conn->exec($sql);
            echo "New sensor created successfully\n";
        }

        //Fazendo o insert das informações do sensor
        echo "Adding info to database\n";
        $sql = "INSERT INTO info (sensorInfo,infoDate,sensorID) VALUES (". $data->{"prediction"} .", '". $data->{"date"} ."', ". $SensorId .")";
        $conn->exec($sql);
        echo "New record created successfully\n";
    }catch(PDOException $e) {
        //Caso o banco retorne algum erro, este é mostrado
        echo $sql . "\n" . $e->getMessage();
      }

    //Encerrando conexão
    $conn = null;
}
else{
}

?>