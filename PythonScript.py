#Importando bibliotecas
import requests
import random
from datetime import datetime

#Variavel para guardar o path para o Endpoint
API_ENDPOINT="http://"

#Recebendo o ip do servidor e adicionando ao path
print("Digite o ip do servidor: ")
ip = input()
if(ip.strip() != ""):
    API_ENDPOINT+=ip.strip()
else:
    raise Exception("IP INVALIDO")

#Recebendo o port do servidor e adicionando ao path
print("Digite o port (Opcional): ")
port = input()
if(port.strip() != ""):
    API_ENDPOINT+=":"+port.strip()


#Recebendo a rota a ser adicionando ao path
print("Insira a rota (Opcional)")
path = input()
if(path.strip() != ""):
    API_ENDPOINT+="/"+path.strip()

#Finalizando o path
API_ENDPOINT+="/"
print(API_ENDPOINT)

#Pegando a data do PC
data = datetime.now().strftime("%Y-%m-%d %H:%M")

#Recebendo o código do sensor
print("Insira o código do sensor: ")
SensorID = input()
if(SensorID.strip() == ""):
    raise Exception("SENSOR INVALIDO")

#Construindo os JSONs a serem enviados
obj = {"date":data, "prediction":str(random.randint(-1000,1000))}
header = {"SensorID": '"'+SensorID.strip()+'"'}

print(obj)
print(header)

#POST request à ENDPOINT construída
requests.post(API_ENDPOINT, headers=header, data=obj)
